<?php

class Steam
{
	private $api_key;
	
	public function __construct($apikey)
	{
		$this->api_key = $apikey;
	}
	
	private function get_data_from_url($url)
	{
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}
		
	private function get_friends_from_id($id)
	{
		$url = "http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=" . $this->api_key . "&steamid=" . $id . "&relationship=friend";
		$result = $this->get_data_from_url($url);
		return $result;
	}

	private function get_friends_data($friends)
	{
		$url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" . $this->api_key . "&steamids=" . $friends;
		$result = $this->get_data_from_url($url);
		return $result;
	}

	private function concat_friends($friendsids)
	{
		$result = array();
		
		foreach($friendsids as $friend)
		{
			$result[] = $friend['steamid'];	
		}

		return $result;
	}
	
	public function get_id_from_name($name)
	{
		$url = "http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=" . $this->api_key . "&vanityurl=" . $name;
		$result = json_decode($this->get_data_from_url($url), true);
		return $result['response']['steamid'];
	}

	public function get_friends($steamid)
	{	
		$friends_data = json_decode($this->get_friends_from_id($steamid), true);
		$friends_data_json = $friends_data['friendslist']['friends'];
		$friends = $this->concat_friends($friends_data_json);
				
		$friends_ids = implode(",", $friends);
		
		$result = $this->get_friends_data($friends_ids);
		
		return $result;
	}
	
	
	public function get_games($id)
	{
		$url = "http://steamcommunity.com/profiles/" . $id . "/games?xml=1";
		$data = new SimpleXMLElement($this->get_data_from_url($url));
		
		$result = array();
		$result['ids'] = array();
		$result['games'] = array();
		
		foreach($data->games->game as $game) {
				$g = array();
				$g['appID'] = (string) $game->appID;
				$g['name']  = (string) $game->name;
				$g['logo'] = (string) $game->logo;
				$g['storeLink'] = (string) $game->storeLink;
				$g['hoursOnRecord'] = (float) $game->hoursOnRecord;
				$g['hoursLast2Weeks'] = (float) $game->hoursLast2Weeks;
				$result['ids'][] = $g['appID'];
				$result['games']["id" . (string)$game->appID] = $g;
		}
		
		return $result;
	
	}

}


?>