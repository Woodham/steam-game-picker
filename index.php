<?php

require "steam.php";

$apikey = "65A537D4637B9049F654CDEF0DBB5217";

$steam = new Steam($apikey);

function print_array($array)
{
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

function throw_error($error)
{
	$result[0] = array();
	$result[0]['error'] = $error;
	echo json_encode($result);
	exit();
}

function get_friends()
{
	global $steam;
	
	$user = $_GET['user'];
	
	if(!$user)
	{
		throw_error("No User Specified");
	}
	
	try
	{
		
		$id = $steam->get_id_from_name($user);
		
		$result = array();
		
		$data = json_decode($steam->get_friends($id),true);
		
		$result['friends'] = $data['response']['players'];
		$result['user'] = $id;
		
	}catch(Exception $e)
	{
		throw_error($e->getMessage());
	}
	return json_encode($result);

}

function get_games()
{
	global $steam;
	
	$idsstring = $_GET["users"];
	
	if(!$idsstring)
	{
		throw_error("No Users Specified");	
	}
	
	$ids = explode(",", $idsstring);

	$games = array();
	$id_arrays = array();
	$result = array();
	
	try
	{
		foreach($ids as $id)
		{
			$data = $steam->get_games($id);
			$games = array_merge($games, $data['games']);
			$id_arrays[] = $data['ids'];
		}

		$intersect = call_user_func_array('array_intersect', $id_arrays);
		
		foreach($intersect as $game_id)
		{
			$result[] = $games["id" . $game_id];
		}
		
	}catch(Exception $e)
	{
		throw_error("The steam api is wonky, try again until it works");	
	}
	return json_encode($result);
}




$method = $_GET["method"];

if(!$method)
{
	throw_error('No Method Specified');
}

switch ($method) {
	case "friends":
		echo get_friends();
		break;
	case "games":
		echo get_games();
		break;
	default:
		throw_error("Method Not Valid");
}


?>