$(function() 
{	

	userid = "";

	function getUserHTML(userObject)
	{
		var html = "";
		html += '<div class="steamUser" id="su-' + userObject.steamid + '" style="width: 250px;height:40px;float:left;margin: 2px; padding:0;">';
		html += '<label for="cb-' + userObject.steamid + '">';
		html += '<a href="' + userObject.profileurl + '" style="display: block;width:100%;height:100%">';
		html += '<div class="steamUserAvatar" style="float:left;width:40px;height:40px">';
		html += '<img src="' + userObject.avatar + '" alt="' + userObject.personaname + '" >';
		html += '</div>';
		html += '</a>';
		html += '<div class="steamUserDetails" style="float:left;width:210px">';
		html += userObject.personaname;
		html += '<input type="checkbox" name="friend" class="friend-checkbox" value="' + userObject.steamid + '" id="cb-' + userObject.steamid + '">';
		html += '</div>';
		html += '</label>';
		html += '</div>';

		return html;
	}
	
	function getGameHTML(gameObject)
	{
		var html = "";
		html += '<div class="steamGame" id="su-' + gameObject.appID + '" style="width: 250px;height:69px;float:left;margin: 2px; padding:0;">';
		html += '<a href="steam://run/' + gameObject.appID + '" style="display: block;width:100%;height:100%">';
		html += '<div class="steamGameLogo" style="width:184px;height:69px;margin: auto auto">';
		html += '<img src="' + gameObject.logo + '" alt="' + gameObject.name + '" >';
		html += '</div>';
		html += '</a>';
		html += '</div>';

		return html;
	}
	
	function getErrorHTML(error)
	{
		var html = '';
		html += '<div class="alert alert-block fade in">';
		html += '<a class="close" data-dismiss="alert">&times;</a>';
		html += '<p>' + error + '</p>';
		html += '</div>';
	
		return html;		
	}
	



	function getFriends(username)
	{
		$('#friends').empty();
		
		$.getJSON("/steam-game-picker-service/?method=friends&user=" + username, function(result)
		{
			var list = '';
			
			userid = result.user;
			
			$.each(result.friends, function(index, item)
			{
				list += getUserHTML(item);
			});
						
			$('#friends').append(list);
		});
	}
	
	function getGames(steamids)
	{
	
		if(steamids.length > 0 && userid != "")
		{
			var idsconcated = steamids.join(',');
			idsconcated = userid + "," + idsconcated;
			
			
			$.getJSON("/steam-game-picker-service/?method=games&users=" + idsconcated, function(result)
			{
				if(!result[0].error)
				{
					$('#games').empty();
				
					var list = '';
					
					$.each(result, function(index, item)
					{
						list += getGameHTML(item);
					});
								
					$('#games').append(list);
				}else
				{
					var errorhtml = getErrorHTML(result[0].error);
					$('#games').prepend(errorhtml);
				}
			
			});
		}
	}

	$("#btnGo").click(function()
	{
		getFriends($("#steamusername").val());
	});
	
	$("#btnGetGames").click(function()
	{
		var ids = new Array();
	
		$(".friend-checkbox:checked").each(function(index, item)
		{
			ids[index] = $(this).val();
		
		});
		
		getGames(ids);
	});

});